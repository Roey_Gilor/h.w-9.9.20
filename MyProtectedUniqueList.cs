﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace H.W_9._9._20
{
    class MyProtectedUniqueList
    {
        private List<string> words;
        private string SecretWord { get; set; }
        public MyProtectedUniqueList(string secretWord)
        {
            words = new List<string>();
            SecretWord = secretWord;
        }
        public void Add (string word)
        {
            if (string.IsNullOrEmpty(word))
                throw new ArgumentNullException("No value was inputed to the word");
            if (words.Contains(word))
                throw new InvalidOperationException("word already is exists in list");
            words.Add(word);
        }
        public void Remove (string word)
        {
            if (string.IsNullOrEmpty(word))
                throw new ArgumentNullException("No value was inputed to the word");
            if (!words.Contains(word))
                throw new ArgumentException("word doesn't exists in list");
            words.Remove(word);
        }
        public void RemoveAt (int index)
        {
            if (index < 0 || index > words.Count)
                throw new ArgumentOutOfRangeException("index was out of list's range");
            words.RemoveAt(index);
        }
        public void Clear (string word)
        {
            if (SecretWord != word)
                throw new AccessViolationException("Wrong secret word was inputed");
            words.Clear();
        }
        public void Sort (string word)
        {
            if (SecretWord != word)
                throw new AccessViolationException("Wrong secret word was inputed");
            words.Sort();
        }
        public override string ToString()
        {
            string st = "";
            foreach (string word in words)
            {
                st += word + " ";
            }
            return st;
        }
    }
}
