﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace H.W_9._9._20
{
    class Program
    {
        static void Main(string[] args)
        {
            MyProtectedUniqueList list = new MyProtectedUniqueList("Mango");
            #region Add
            try
            {
                list.Add(null);
                Console.WriteLine("word was inputed successfully");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            try
            {
                list.Add("bannana");
                Console.WriteLine("word was inputed successfully");
            }
            catch (Exception e)
            {

                Console.WriteLine(e.Message);
            }
            try
            {
                list.Add("bannana");
                Console.WriteLine("word was inputed successfully");
            }
            catch (Exception e)
            {

                Console.WriteLine(e.Message);
            }
            #endregion
            #region Remove
            try
            {
                list.Remove("ball");
                Console.WriteLine("word was removed successfully");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            try
            {
                list.Remove("bannana");
                Console.WriteLine("word was removed successfully");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            #endregion
            #region Adding words to list
            try
            {
                list.Add("bannana");
                Console.WriteLine("word was inputed successfully");
            }
            catch (Exception e)
            {

                Console.WriteLine(e.Message);
            }           
            try
            {
                list.Add("Shoko");
                Console.WriteLine("word was inputed successfully");
            }
            catch (Exception e)
            {

                Console.WriteLine(e.Message);
            }
            try
            {
                list.Add("Melon");
                Console.WriteLine("word was inputed successfully");
            }
            catch (Exception e)
            {

                Console.WriteLine(e.Message);
            }
            try
            {
                list.Add("bird");
                Console.WriteLine("word was inputed successfully");
            }
            catch (Exception e)
            {

                Console.WriteLine(e.Message);
            }
            #endregion
            #region RemoveAt
            try
            {
                list.RemoveAt(-1);
                Console.WriteLine("Item was removed in this place successfully");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            try
            {
                list.RemoveAt(2);
                Console.WriteLine("Item was removed in this place successfully");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            #endregion
            #region Sort
            try
            {
                list.Sort("ballon");
                Console.WriteLine(list.ToString());
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            try
            {
                list.Sort("Mango");
                Console.WriteLine(list.ToString());
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            #endregion
            #region Clear
            try
            {
                list.Clear("boom");
                Console.WriteLine("list is clear");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            try
            {
                list.Clear("Mango");
                Console.WriteLine("list is clear");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            #endregion
        }
    }
}
